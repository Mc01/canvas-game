/*global document, window*/

var id = {
    'canvasID': 'glCanvas',
    'contentID': 'glContent',
    'debugID': 'glDebug',
    'frameID': 'glFrame',
    'wrapperID': 'glWrapper'
};

var config = {
    'canvasWidth': 0.9,
    'canvasHeight': 0.65,
    'frameHeight': 0.05,
    'frameRate': 1000 / 100,
    'wrapperHeight': 0.25
};

function rebuildFrame() {
    'use strict';
    return window.innerHeight * config.frameHeight;
}

function rebuildWidth() {
    'use strict';
    return window.innerWidth * config.canvasWidth;
}

function rebuildHeight() {
    'use strict';
    return window.innerHeight * config.canvasHeight;
}

function rebuildWrapper() {
    'use strict';
    return window.innerHeight * config.wrapperHeight;
}

var constants = {
    'canvasHTML': '<canvas id="' + id.canvasID + '" width="' + rebuildWidth() + '" height="' + rebuildHeight() + '"></canvas>',
    'debugHTML': '<div id="' + id.debugID + '" style="margin-left:2px">',
    'divHTML': '</div>',
    'frameHTML': '<div id="' + id.frameID + '" height="' + rebuildFrame() + '" style="margin-left:2px; color:#ff0000">',
    'resizeEvent': 'resize',
    'webGL': 'webgl',
    'wrapperHTML': '<div id="' + id.wrapperID + '" style="background-color:#EEEEEE; overflow-y:scroll; height:' + rebuildWrapper() + 'px" align="left">'
};

var messages = {
    'resizeWindow': 'Window was resized to: ',
    'webGLFailure': 'WebGL failed to initialize.',
    'webGLSuccess': 'WebGL successfully initialized.'
};

var canvas;
var debug;
var debugStack;
var frame;
var gl;
var last;
var stack;
var wrapper;

function initContent() {
    'use strict';
    var content = document.getElementById(id.contentID);
    content.innerHTML = constants.canvasHTML + constants.wrapperHTML + constants.frameHTML + constants.divHTML + constants.debugHTML + constants.divHTML + constants.divHTML;
    canvas = document.getElementById(id.canvasID);
    frame = document.getElementById(id.frameID);
    wrapper = document.getElementById(id.wrapperID);
    debug = document.getElementById(id.debugID);
    debugStack = '';
}

function clamp(value) {
    'use strict';
    if (value < 10) {
        value = '0' + value;
    }
    return value;
}

function trace(string) {
    'use strict';
    var date = new Date(),
        hours = clamp(date.getHours()),
        minutes = clamp(date.getMinutes()),
        seconds = clamp(date.getSeconds()),
        timeStamp = '(' + hours + ':' + minutes + ':' + seconds + ') ';
    debugStack = timeStamp + string + '<br />' + debugStack;
    debug.innerHTML = constants.debugHTML + debugStack + constants.divHTML;
}

function initGL(canvas) {
    'use strict';
    var gl = null;
    gl = canvas.getContext(constants.webGL);
    if (gl) {
        trace(messages.webGLSuccess);
    } else {
        trace(messages.webGLFailure);
        gl = null;
    }
    return gl;
}

function resize() {
    'use strict';
    canvas.width = rebuildWidth();
    canvas.height = rebuildHeight();
    wrapper.style.minHeight = rebuildWrapper() + 'px';
    wrapper.style.maxHeight = rebuildWrapper() + 'px';
    trace(messages.resizeWindow + window.innerWidth + 'x' + window.innerHeight);
}

function updateFrame() {
    'use strict';
    var time = new Date().getSeconds();
    if (last != time) {
        last = time;
        var string = 'Tick[' + clamp(last) + ']: ' + stack;
        frame.innerHTML = constants.frameHTML + string + constants.divHTML;
        stack = 0;
    } else {
        stack++;
    }
}

function tick() {
    'use strict';
    updateFrame();
    if (gl) {
        gl.clear(gl.COLOR_BUFFER_BIT);
    }
}

function init() {
    'use strict';
    initContent();
    gl = initGL(canvas);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    window.addEventListener(constants.resizeEvent, resize);
    window.setInterval(tick, config.frameRate);
}

init();